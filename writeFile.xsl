<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:param name="file1" select="document('test.xml')"/>

  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="/">
      <xsl:for-each select="/article/*">
    <xsl:result-document method="xml" href="file_{name(.)}-output.xml">
        <xsl:copy-of select="../* | ." />
    </xsl:result-document>
  </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
