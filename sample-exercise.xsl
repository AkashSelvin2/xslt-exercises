<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:mml="http://www.w3.org/1998/Math/MathML"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<xsl:output method="xml" indent = "yes" />
<xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>
    <xsl:template match="article">
        <html>
        <xsl:apply-templates select="@* | node()" />
        </html>
    </xsl:template>
    <xsl:template match="front|back|workflow|production-notes"/>
    <xsl:template match="//title[@data-class]">
            <xsl:if test = '@data-class = "jrnlHead1"'> 
                <h1>
                <xsl:apply-templates select="@*"/>
                <xsl:apply-templates select="text()" />
                </h1>
            </xsl:if>
            <xsl:if test = '@data-class = "jrnlHead2"'> 
                <h2>
                <xsl:apply-templates select="@*"/>
                <xsl:apply-templates select="text()" />
                </h2>
            </xsl:if> 
            <xsl:if test = '@data-class = "jrnlHead3"'> 
               <h3>
               <xsl:apply-templates select="@*"/>
               <xsl:apply-templates select="text()" />
               </h3>
            </xsl:if> 
            <xsl:if test = '@data-class = "jrnlHead4"'> 
                <h4>
                <xsl:apply-templates select="@*"/>
                <xsl:apply-templates select="text()" />
                </h4>
            </xsl:if>
    </xsl:template>
    <xsl:template match="//fig">
    <xsl:variable name="Label" select="label/*"/>
    <xsl:variable name="Caption" select="caption/*"/>
    <div class="jrnlFigBlock">
        <xsl:apply-templates select='*except (label, caption)' />
        <div class="jrnlFigCaption">
        <xsl:apply-templates select="$Label" />
        <xsl:apply-templates select="$Caption" />
    </div>
    </div>
    </xsl:template>
        <xsl:template match="//fig/graphic">
	<img>
        <xsl:apply-templates select="@* | node()" />
    </img>
		</xsl:template>
</xsl:stylesheet>
